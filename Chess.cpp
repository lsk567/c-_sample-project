#undef FOR_RELEASE

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include "Game.h"
#include "Chess.h"
#include "Prompts.h"
#include "Terminal.h"
using namespace std;

// Make a move on the board. Return an int, with < 0 being failure
int ChessGame::makeMove(Position start, Position end) {
    // Possibly implement chess-specific move logic here
    //
    // We call Board's makeMove to handle any general move logic
    // Feel free to use this or change it as you see fit
    int retCode = Board::makeMove(start, end);
    return retCode;
}

// Setup the chess board with its initial pieces
void ChessGame::setupBoard() {
    std::vector<int> pieces {
        ROOK_ENUM, KNIGHT_ENUM, BISHOP_ENUM, QUEEN_ENUM,
        KING_ENUM, BISHOP_ENUM, KNIGHT_ENUM, ROOK_ENUM
    };
    for (size_t i = 0; i < pieces.size(); ++i) {
        initPiece(PAWN_ENUM, WHITE, Position(i, 1));
        initPiece(pieces[i], WHITE, Position(i, 0));
        initPiece(pieces[i], BLACK, Position(i, 7));
        initPiece(PAWN_ENUM, BLACK, Position(i, 6));
    }
}

int main() {
    ChessGame chess;
    chess.run();
    chess.setupBoard();

    
    Terminal T;
    for(int i=0;i<8;i++){
     for(int k=0;k<3;k++){
      for(int j=0;j<8;j++){
        if( (i%2!=0 && j%2!=0)||(i%2==0 && j%2==0) ){
          T.colorAll(false,Terminal::RED,Terminal::GREEN);
        }
        else{
          T.colorAll(false,Terminal::RED,Terminal::RED);
        }

        if(k==1){
          Piece* tmp=chess.getPiece(Position(j,i));
          cout<<" ";
          if (tmp!=nullptr){
            if(tmp->m_owner==0){
              T.colorFg(true,Terminal::WHITE);
            }
            if(tmp->m_owner==1){
              T.colorFg(true,Terminal::BLACK);
            }
            cout<<PieceSymbol(tmp->id());
          }      
          else{
            cout<<" ";
          }
          cout<<" ";
        }
        else{
          cout<<"   ";
        }
        T.set_default();
      }
      printf("\n");
     }
    }

}
